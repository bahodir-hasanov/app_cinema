package uz.pdp.app_cinema.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.app_cinema.model.MovieAnnouncement;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Projection(types = {MovieAnnouncement.class})
public interface MovieSessionProjection {
//    UUID getId(); // sians(session) id

    UUID getMovieAnnouncementId(); // afisha(announcement) id

    UUID getMovieId();

    String getMovieTitle();

    UUID getMovieCoverImgId();

    UUID getStartDateId();

    LocalDate getStartDate();

    @Value("#{@hallRepository.getHallsAndTimesByMovieAnnouncementIdAndStartDateId(target.movieAnnouncementId, target.startDateId)}")
    List<HallAndTimesProjectionForSession> getHalls();

}
