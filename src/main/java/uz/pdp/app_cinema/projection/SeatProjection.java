package uz.pdp.app_cinema.projection;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.app_cinema.model.Hall;
import uz.pdp.app_cinema.model.Row;
import uz.pdp.app_cinema.model.Seat;

// Bahodir Hasanov 3/15/2022 9:51 AM
@Projection(types = {Seat.class})
public interface SeatProjection {
    Integer getId();
    Integer getNumber();
}
