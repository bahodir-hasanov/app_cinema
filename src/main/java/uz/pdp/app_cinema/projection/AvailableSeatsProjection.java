package uz.pdp.app_cinema.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.app_cinema.model.Hall;

import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/26/2022 11:27 AM
@Projection(types = {Hall.class})
public interface AvailableSeatsProjection {
    UUID getId();
    String getName();
    @Value("#{@rowRepository.getSeatRowsByHallId(target.id)}")
    List<AvailableRowsProjection> getRows();
}

