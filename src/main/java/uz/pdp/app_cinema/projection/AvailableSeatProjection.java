package uz.pdp.app_cinema.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.app_cinema.model.Seat;

import java.util.UUID;

// Bahodir Hasanov 3/26/2022 11:27 AM
@Projection(types = {Seat.class})
public interface AvailableSeatProjection {
    UUID getId();

    Integer getNumber();

    UUID getPriceCategoryId();

    String getPriceCategoryName();

    String getColor();

}

