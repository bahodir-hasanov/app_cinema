package uz.pdp.app_cinema.projection;
// Bahodir Hasanov 3/24/2022 3:40 PM

import java.time.LocalTime;
import java.util.UUID;

public interface SessionTimeProjection {
    UUID getId();

    LocalTime getTime();

    UUID getSessionId();
}
