package uz.pdp.app_cinema.projection;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.app_cinema.model.Hall;
import uz.pdp.app_cinema.model.Row;

// Bahodir Hasanov 3/15/2022 9:51 AM
@Projection(types = {Row.class, Hall.class})
public interface RowProjection {
    Integer getId();
    Integer getNumber();
    String getName();
}
