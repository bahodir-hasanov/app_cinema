package uz.pdp.app_cinema.utils;
// Bahodir Hasanov 3/16/2022 4:18 PM

public interface Constants {
    String DEFAULT_PAGE_SIZE = "3";
    Integer THIRTY_MINUTES_IN_MILLISECOND = 1_800_000;

}
