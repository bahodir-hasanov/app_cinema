package uz.pdp.app_cinema.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

// Bahodir Hasanov 3/14/2022 6:27 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "price_categories")
@EqualsAndHashCode(callSuper = true)
public class PriceCategory extends AbsEntity {
    private String name;
    private Double additionalFeeInPercent;
    private String color;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "priceCategory")
    private List<Seat> seats;

    public PriceCategory(String name, Double addAddFeeInPercent, String color) {
        this.name = name;
        this.additionalFeeInPercent = addAddFeeInPercent;
        this.color = color;
    }

}
