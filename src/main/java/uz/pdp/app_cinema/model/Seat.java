package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;

// Bahodir Hasanov 3/14/2022 6:55 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "seats")
@EqualsAndHashCode(callSuper = true)
public class Seat extends AbsEntity {
    private Integer number;
    @ManyToOne
    private Row row;
    @ManyToOne
    private PriceCategory priceCategory;
}
