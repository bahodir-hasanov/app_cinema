package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.enums.CastType;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;

// Bahodir Hasanov 3/17/2022 9:01 AM

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "casts")
@EqualsAndHashCode(callSuper = true)
public class Cast extends AbsEntity {
    @Column(nullable = false)
    private String fullName;
    @OneToOne
    private Attachment photo;
    @Enumerated(EnumType.STRING)
    private CastType castType;
}
