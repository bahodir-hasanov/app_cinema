package uz.pdp.app_cinema.model;
// Bahodir Hasanov 3/29/2022 8:28 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.enums.TicketStatus;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "transaction_histories")
public class TransactionHistory extends AbsEntity {
    @ManyToMany
    @JoinTable(name = "transactions_histories_tickets",
            joinColumns = {@JoinColumn(name = "transaction_history_id")},
            inverseJoinColumns = {@JoinColumn(name = "ticket_id")})
    private List<Ticket> ticketList;
    @OneToOne
    private PayType payType;
    private double totalAmount;
    @Enumerated(value = EnumType.STRING)
    private TicketStatus status;
    private String paymentIntent;
}
