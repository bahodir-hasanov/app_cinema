package uz.pdp.app_cinema.model;

// Bahodir Hasanov 3/14/2022 10:38 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "movies")
@EqualsAndHashCode(callSuper = true)
public class Movie extends AbsEntity {

    private String title; //

    private String description; //

    private Integer durationInMinutes; //

    private double minPrice; //

    @OneToOne(cascade = CascadeType.MERGE)
    private Attachment coverImage;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<Attachment> photos;

    private String trailerVideoUrl; // ex. youtube link

    private LocalDate releaseDate;

    private double budget;

    @ManyToOne
    private Distributor distributor;

    protected double distributorShareInPercent;

    @ManyToMany
    @JoinTable(name = "movies_casts",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "cast_id")})
    private List<Cast> casts;

    @ManyToMany
    @JoinTable(name = "movies_genres",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")}
    )
    private List<Genre> genres;


    public Movie(String title, String description, Integer durationInMinutes, double minPrice, Attachment coverImage, String trailerVideoUrl, LocalDate releaseDate, double budget, Distributor distributor, double distributorShareInPercent, List<Cast> casts, List<Genre> genres) {
        this.title = title;
        this.description = description;
        this.durationInMinutes = durationInMinutes;
        this.minPrice = minPrice;
        this.coverImage = coverImage;
        this.trailerVideoUrl = trailerVideoUrl;
        this.releaseDate = releaseDate;
        this.budget = budget;
        this.distributor = distributor;
        this.distributorShareInPercent = distributorShareInPercent;
        this.casts = casts;
        this.genres = genres;
    }
}
