package uz.pdp.app_cinema.model;
// Bahodir Hasanov 3/17/2022 12:41 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity(name = "movies_sessions")
public class MovieSession extends AbsEntity {
    @ManyToOne
    MovieAnnouncement movieAnnouncement;
    @ManyToOne
    Hall halls;
    @ManyToOne
    SessionDate startDate;
    @ManyToOne
    SessionTime startTime;
    @ManyToOne
    SessionTime endTime;
}
