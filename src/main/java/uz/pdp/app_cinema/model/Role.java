package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.Set;

// Bahodir Hasanov 3/14/2022 6:59 PM

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "roles")
@PackagePrivate
public class Role extends AbsEntity {
    @Column(nullable = false,unique = true)
    private String name;
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(
            name = "roles_permissions",
            joinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "permission_id", referencedColumnName = "id"))
    private Set<Permission> permissions;

}
