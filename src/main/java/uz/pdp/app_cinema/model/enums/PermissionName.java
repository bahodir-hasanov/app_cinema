package uz.pdp.app_cinema.model.enums;

public enum PermissionName {
    CAN_ADD_HOLE,
    CAN_DELETE_USER,
    CAN_ADD_MOVIE
}
