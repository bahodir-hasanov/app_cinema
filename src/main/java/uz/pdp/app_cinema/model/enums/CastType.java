package uz.pdp.app_cinema.model.enums;

public enum CastType {
    ACTOR,
    DIRECTOR
}
