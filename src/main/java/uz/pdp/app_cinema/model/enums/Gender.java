package uz.pdp.app_cinema.model.enums;

public enum Gender {
    MALE,
    FEMALE;
}
