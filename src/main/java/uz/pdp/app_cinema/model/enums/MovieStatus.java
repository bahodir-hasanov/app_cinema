package uz.pdp.app_cinema.model.enums;

public enum MovieStatus {
    ACTIVE,
    DISABLE,
    PASSED,
    SOON
}
