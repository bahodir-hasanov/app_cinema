package uz.pdp.app_cinema.model.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER
}
