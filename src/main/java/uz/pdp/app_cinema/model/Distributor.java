package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.List;

// Bahodir Hasanov 3/14/2022 6:01 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity(name = "distributors")
public class Distributor extends AbsEntity {

    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "text")

    private String description;

}
