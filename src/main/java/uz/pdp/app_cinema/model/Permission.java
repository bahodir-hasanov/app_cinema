package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

// Bahodir Hasanov 3/14/2022 7:01 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "permissions")
public class Permission extends AbsEntity {
    @Column(nullable = false)
   private String name;
}
