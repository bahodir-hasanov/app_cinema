package uz.pdp.app_cinema.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

// Bahodir Hasanov 3/14/2022 6:17 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity(name = "halls")
public class Hall extends AbsEntity {
    @Column(nullable = false, length = 50)
    private String name;
    @JsonIgnore
    @OneToMany(mappedBy = "hall", cascade = CascadeType.ALL)
    private List<Row> rows;
    private double vipAdditionalFeeInPercent;

    public Hall(String name) {
        this.name = name;
    }
    public Hall(String name, double vipAdditionalFeeInPercent) {
        this.name = name;
        this.vipAdditionalFeeInPercent = vipAdditionalFeeInPercent;
    }
}

