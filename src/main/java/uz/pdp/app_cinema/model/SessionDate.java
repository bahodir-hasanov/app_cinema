package uz.pdp.app_cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.app_cinema.model.template.AbsEntity;

import javax.persistence.Entity;
import java.time.LocalDate;


// Bahodir Hasanov 3/17/2022 12:55 PM

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "session_date")
@EqualsAndHashCode(callSuper = true)
public class SessionDate extends AbsEntity {
    private LocalDate date;
}
