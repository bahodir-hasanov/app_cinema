package uz.pdp.app_cinema.repository;
// Bahodir Hasanov 3/16/2022 11:38 AM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.Actor;

import java.util.UUID;

public interface ActorRepository extends JpaRepository<Actor, UUID> {
}
