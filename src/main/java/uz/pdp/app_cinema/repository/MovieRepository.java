package uz.pdp.app_cinema.repository;
// Bahodir Hasanov 3/16/2022 3:51 PM

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.app_cinema.model.Movie;
import uz.pdp.app_cinema.projection.MovieProjection;

import java.util.UUID;

@Repository
public interface MovieRepository extends JpaRepository<Movie, UUID> {
    @Query(value = "SELECT cast(m.id as varchar) as id," +
            "m.title," +
            "cast(m.release_date as date) as releaseDate,\n" +
            "cast(m.cover_image_id as varchar) as coverImageId " +
            "from movies m where lower(title)\n" +
            "like (concat('%', :search, '%'))", nativeQuery = true)
    Page<MovieProjection> findAllByPage(Pageable pageable, @Param("search") String search);

}
