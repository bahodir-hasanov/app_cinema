package uz.pdp.app_cinema.repository;
// Bahodir Hasanov 3/15/2022 10:39 PM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.Distributor;

import java.util.UUID;

public interface DistributorRepository extends JpaRepository<Distributor, UUID> {
}
