package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.app_cinema.model.Row;
import uz.pdp.app_cinema.projection.AvailableRowsProjection;
import uz.pdp.app_cinema.projection.RowProjection;

import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/15/2022 9:34 AM

public interface RowRepository extends JpaRepository<Row, UUID> {
    @Query(value = "select cast(r.id as varchar) as id, r.number, h.name as Name from hall_rows r join halls h on r.hall_id=h.id where h.id = :hall_id", nativeQuery = true)
    List<RowProjection> getRowsByHallId(UUID hall_id);

    @Query(value = "select cast(r.id as varchar) as id," +
            " r.number " +
            "from hall_rows r " +
            "join halls h on r.hall_id=h.id where h.id =:hall_id", nativeQuery = true)
    List<AvailableRowsProjection> getSeatRowsByHallId(UUID hall_id);

}
