package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.app_cinema.model.NightSessionAddFee;

import java.time.LocalTime;
import java.util.UUID;

@Repository
public interface NightSessionAddFeeRepository extends JpaRepository<NightSessionAddFee, UUID> {
    @Query(value = "select st.time from night_session_add_fee ns\n" +
            "join session_time st on ns.time_id = st.id order by\n" +
            "    ns.updated_at desc limit 1 offset 0",nativeQuery = true)
     LocalTime getLastTimeChanges();


    @Query(value = "select ns.percentage from night_session_add_fee ns\n" +
            "join session_time st on ns.time_id = st.id order by\n" +
            "    ns.updated_at desc limit 1 offset 0",nativeQuery = true)
    double getLastTimeChangePercentage();
}
