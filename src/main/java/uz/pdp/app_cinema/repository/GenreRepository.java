package uz.pdp.app_cinema.repository;
// Bahodir Hasanov 3/16/2022 10:07 PM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.Genre;

import java.util.UUID;

public interface GenreRepository extends JpaRepository<Genre, UUID> {
}
