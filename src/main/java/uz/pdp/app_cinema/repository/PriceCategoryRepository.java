package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.PriceCategory;

import java.util.UUID;

public interface PriceCategoryRepository extends JpaRepository<PriceCategory, UUID> {
}
