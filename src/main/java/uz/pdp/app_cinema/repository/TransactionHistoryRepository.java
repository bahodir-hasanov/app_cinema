package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.app_cinema.model.TransactionHistory;

import java.util.Optional;
import java.util.UUID;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, UUID> {
    @Query(value = "select count(*) from transaction_histories where created_at::date =TO_DATE(:date,'YYYY-MM-DD')", nativeQuery = true)
    Integer getCountOfAllSoledTicketDaily(String date);

    @Query(value = "select ph.*\n" +
            "from transaction_histories ph join transactions_histories_tickets pht on ph.id = pht.transaction_history_id\n" +
            "join tickets t on pht.ticket_id = t.id where t.id =:ticketId", nativeQuery = true)
    Optional<TransactionHistory> findTransactionHistoryByTicketId(UUID ticketId);

    @Query(nativeQuery = true, value = "select th.payment_intent\n" +
            "from transaction_histories th\n" +
            "join transactions_histories_tickets tht\n" +
            "    on th.id = tht.transaction_history_id\n" +
            "where tht.ticket_id=:ticketId and th.status = 'PURCHASED'")
    String getPaymentIntentByTicketId(UUID ticketId);
}
