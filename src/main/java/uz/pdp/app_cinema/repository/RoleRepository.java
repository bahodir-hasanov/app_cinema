package uz.pdp.app_cinema.repository;
// Bahodir Hasanov 4/6/2022 9:47 AM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.Role;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
}
