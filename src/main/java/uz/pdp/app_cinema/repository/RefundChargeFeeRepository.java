package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.app_cinema.model.RefundChargeFee;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RefundChargeFeeRepository extends JpaRepository<RefundChargeFee, UUID> {

    @Query(nativeQuery = true, value = "(select rf.percentage\n" +
            "from refund_charge_fee rf\n" +
            "where  rf.interval_minutes>=:minute\n" +
            "order by interval_minutes , updated_at DESC\n" +
            "LIMIT 1)\n" +
            "union\n" +
            "(select rf.percentage\n" +
            "from refund_charge_fee rf\n" +
            "order by interval_minutes desc, updated_at DESC\n" +
            "LIMIT 1) order by percentage desc limit 1")
    double findPercentageWithIntervalMinutes(Long minute);

}
