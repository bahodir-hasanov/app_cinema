package uz.pdp.app_cinema.repository;
// Bahodir Hasanov 3/17/2022 3:44 PM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.Cast;

import java.util.UUID;

public interface CastRepository extends JpaRepository<Cast, UUID> {


}
