package uz.pdp.app_cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.MovieAnnouncement;

import java.util.UUID;

public interface MovieAnnouncementRepository extends JpaRepository<MovieAnnouncement, UUID> {


}
