package uz.pdp.app_cinema.repository;

// Bahodir Hasanov 3/15/2022 4:28 PM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.app_cinema.model.AttachmentContent;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    AttachmentContent findByAttachmentId(UUID attachment_id);
    void deleteByAttachmentId(UUID attachment_id);
}
