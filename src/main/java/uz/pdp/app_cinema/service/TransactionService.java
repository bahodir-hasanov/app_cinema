package uz.pdp.app_cinema.service;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Refund;
import com.stripe.model.checkout.Session;
import com.stripe.param.RefundCreateParams;
import com.stripe.param.checkout.SessionCreateParams;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.dto.TicketDto;
import uz.pdp.app_cinema.model.TransactionHistory;
import uz.pdp.app_cinema.model.Ticket;
import uz.pdp.app_cinema.model.User;
import uz.pdp.app_cinema.model.enums.TicketStatus;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.projection.TicketProjection;
import uz.pdp.app_cinema.repository.RefundChargeFeeRepository;
import uz.pdp.app_cinema.repository.TransactionHistoryRepository;
import uz.pdp.app_cinema.repository.TicketRepository;
import uz.pdp.app_cinema.repository.UserRepository;

import java.time.*;
import java.util.*;

// Bahodir Hasanov 3/30/2022 11:55 AM

@Service
@Transactional
@RequiredArgsConstructor
public class TransactionService {
    private final UserRepository userRepository;
    private final TicketRepository ticketRepository;
    private final TransactionHistoryRepository transactionHistoryRepository;
    private final RefundChargeFeeRepository refundChargeFeeRepository;
    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;

    public HttpEntity<?> createStripeSession() {

        Stripe.apiKey = stripeApiKey;

        User currentUser = userRepository.findByUsername("user");

        List<TicketProjection> tickets = ticketRepository.getTicketByUserId(currentUser.getId());

        List<SessionCreateParams.LineItem> lineItems = new ArrayList<>();

        for (TicketProjection ticket : tickets) {

            String movieTitle = ticket.getTitle();

            Double ticketPrice = ticket.getPrice();

            SessionCreateParams.LineItem.PriceData.ProductData productData = SessionCreateParams.LineItem.PriceData.ProductData
                    .builder()
                    .setName(movieTitle)
                    //todo
                    // .setDescription()
                    .build();

            SessionCreateParams.LineItem.PriceData priceData = SessionCreateParams.LineItem.PriceData
                    .builder()
                    .setProductData(productData)
                    .setCurrency("usd")
                    .setUnitAmount((long) ((ticketPrice * 100 + 30) / (1 - 2.9 / 100)))
                    .build();

            SessionCreateParams.LineItem lineItem = SessionCreateParams.LineItem
                    .builder()
                    .setPriceData(priceData)
                    .setQuantity(1L)
                    .build();

            lineItems.add(lineItem);

        }
        SessionCreateParams params = SessionCreateParams
                .builder()
                .setMode(SessionCreateParams.Mode.PAYMENT)
                .setCancelUrl("http://localhost:8080/failed")
                .setSuccessUrl("http://localhost:8080/success")
                .addAllLineItem(lineItems)
                .setClientReferenceId(currentUser.getId().toString())
                .build();
        try {
            Session session = Session.create(params);
            String checkoutURL = session.getUrl();
            return ResponseEntity.ok(checkoutURL);
        } catch (StripeException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }

    public HttpEntity<?> refundTicket(List<UUID> ticketIds) {
        Stripe.apiKey = stripeApiKey;
        double refundTicketsPrice = 0;
        List<Ticket> tickets = new ArrayList<>();
        for (UUID ticketId : ticketIds) {
            Ticket ticketById = ticketRepository.findTicketById(ticketId);
            if (ticketById.getMovieSession().getStartDate().getDate().isBefore(LocalDate.now())) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("several tickets' time expired", false));
            }
            if (ticketById.getMovieSession().getStartTime().getTime().isBefore(LocalTime.now())) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("several tickets' time expired", false));
            }
            LocalDateTime movieSessionDate = LocalDateTime.of(ticketById.getMovieSession().getStartDate().getDate(), ticketById.getMovieSession().getStartTime().getTime());
            Duration duration = Duration.between(LocalDateTime.now(), movieSessionDate);
            Long intervalMinutes = duration.getSeconds() / 60;
            double percentage = refundChargeFeeRepository.findPercentageWithIntervalMinutes(intervalMinutes);
            refundTicketsPrice += (ticketById.getPrice() - (ticketById.getPrice() * percentage / 100));
        }

        String paymentIntent = transactionHistoryRepository.getPaymentIntentByTicketId(ticketIds.get(0));
        RefundCreateParams params = RefundCreateParams
                .builder()
                .setPaymentIntent(paymentIntent)
                .setAmount(((long) refundTicketsPrice))
                .build();

        try {
            Refund refund = Refund.create(params);
            if (refund.getStatus().equals("succeeded")) {
                for (UUID ticketId : ticketIds) {
                    Ticket ticket = ticketRepository.findTicketById(ticketId);
                    ticket.setStatus(TicketStatus.REFUNDED);
                    ticketRepository.save(ticket);
                    tickets.add(ticket);
                }
                transactionHistoryRepository.save(new TransactionHistory(tickets,null,refundTicketsPrice,TicketStatus.REFUNDED,paymentIntent));
                return ResponseEntity.ok(new ApiResponse("successfully refund", true));
            }
        } catch (StripeException e) {
            e.printStackTrace();
        }
        return ResponseEntity.internalServerError().body(new ApiResponse("error",false));
    }

    public HttpEntity<?> firstRefundTicket(List<TicketDto> ticketDtoList) {
        Stripe.apiKey = stripeApiKey;

        Refund refund = null;
        for (TicketDto ticketDto : ticketDtoList) {
            TransactionHistory transactionHistoryByTicketId = transactionHistoryRepository.findTransactionHistoryByTicketId(ticketDto.getTicketId()).orElseThrow(() ->
                    new IllegalStateException("Not found"));
            String stripePaymentIntent = transactionHistoryByTicketId.getPaymentIntent();
            RefundCreateParams params = RefundCreateParams
                    .builder()
                    .setPaymentIntent(stripePaymentIntent)
                    .build();

            try {
                refund = Refund.create(params);
                System.out.println(refund.getStatus());
                if (refund.getStatus().equals("succeeded")) {
                    Optional<Ticket> byId = ticketRepository.findById(ticketDto.getTicketId());
                    Ticket ticket = byId.get();
                    ticket.setStatus(TicketStatus.REFUNDED);
                    ticketRepository.save(ticket);
                    Optional<TransactionHistory> transactionByTicketId1 = transactionHistoryRepository.findTransactionHistoryByTicketId(ticketDto.getTicketId());
                    TransactionHistory transactionHistory = transactionByTicketId1.get();
                    transactionHistory.setStatus(TicketStatus.REFUNDED);
                    transactionHistory.setId(null);
                    transactionHistoryRepository.save(transactionHistory);
                    return ResponseEntity.ok(new ApiResponse("successfully refund", true));
                }
            } catch (StripeException e) {
                e.printStackTrace();
            }
        }
        return ResponseEntity.ok(refund.getStatus());
    }

}
