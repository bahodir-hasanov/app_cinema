package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.app_cinema.model.Attachment;
import uz.pdp.app_cinema.model.AttachmentContent;
import uz.pdp.app_cinema.model.Cast;
import uz.pdp.app_cinema.repository.AttachmentContentRepository;
import uz.pdp.app_cinema.repository.AttachmentRepository;
import uz.pdp.app_cinema.repository.CastRepository;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

// Bahodir Hasanov 3/17/2022 3:43 PM
@Service
public class CastService {
    @Autowired
    CastRepository castRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    public Cast saveCast(MultipartFile file, Cast cast) {
        try {
            Attachment attachment = attachmentRepository.save(new Attachment(file.getContentType(), file.getSize(), file.getOriginalFilename()));
            attachmentContentRepository.save(new AttachmentContent(attachment, file.getBytes()));
            castRepository.save(new Cast(cast.getFullName(), attachment, cast.getCastType()));
        } catch (Exception e) {
            return null;
        }
        return castRepository.save(cast);
    }

    public Cast getCastById(UUID id) {
        Optional<Cast> byId = castRepository.findById(id);
        if (byId.isPresent()) {
            return byId.get();
        }
        return null;
    }

    public List<Cast> getAllCast() {
        return castRepository.findAll();
    }

    public boolean deleteById(UUID id) {
        Optional<Cast> byId = castRepository.findById(id);
        if (byId.isPresent()) {
            castRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean update(MultipartFile file, Cast cast) throws IOException {
        Optional<Cast> castOp = castRepository.findById(cast.getId());
        Cast castNew = null;
        if (castOp.isPresent()) {
            if (!file.isEmpty()) {
                Attachment attachment = castOp.get().getPhoto();
                if (!attachment.equals(null)) {
                    attachment.setSize(file.getSize());
                    attachment.setName(file.getName());
                    attachment.setContent_type(file.getContentType());
                    UUID id = attachment.getId();
                    AttachmentContent byAttachmentId = attachmentContentRepository.findByAttachmentId(id);
                    byAttachmentId.setAttachment(attachment);
                    byAttachmentId.setBytes(file.getBytes());
                    attachmentContentRepository.save(byAttachmentId);
                    attachmentRepository.save(attachment);
                    try {
                        byAttachmentId.setBytes(file.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Attachment save = attachmentRepository.save(new Attachment(file.getContentType(), file.getSize(), file.getOriginalFilename()));
                    attachmentContentRepository.save(new AttachmentContent(save, file.getBytes()));
                    castNew.setPhoto(save);
                }
            }
            castNew.setFullName(cast.getFullName());
            castNew.setCastType(cast.getCastType());
            Cast CastSaved = castRepository.save(castNew);
            if (castNew.equals(null)) return false;
            else return true;
        }
        return false;
    }
}
