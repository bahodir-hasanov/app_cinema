package uz.pdp.app_cinema.service;
// Bahodir Hasanov 3/15/2022 10:36 PM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.model.Distributor;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.DistributorRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class DistributorService {
    @Autowired
    DistributorRepository distributorRepository;

    public ApiResponse getAllDistributors() {
        List<Distributor> distributors = distributorRepository.findAll();
        return new ApiResponse("distributors", true, distributors);
    }

    public ApiResponse savaDistributor(Distributor distributor) {
        Distributor savedDistributor = distributorRepository.save(distributor);
        return new ApiResponse("saved", true, savedDistributor);
    }

    public ApiResponse getDistributorById(UUID id) {
        Optional<Distributor> distributor = distributorRepository.findById(id);
        if (distributor.isPresent()) {
            return new ApiResponse("successfully found", true, distributor.get());
        }
        return new ApiResponse("not found", false,null);
    }

    public ApiResponse updateDistributorById(UUID id, Distributor distributor) {
        Optional<Distributor> distributor1 = distributorRepository.findById(id);
        if (distributor1.isPresent()) {
            Distributor dis = distributor1.get();
            dis.setName(distributor.getName());
            dis.setDescription(distributor.getDescription());
            return new ApiResponse("successfully found", true, dis);
        }
        return new ApiResponse("not found", false);
    }

    public ApiResponse deleteDistributor(UUID id) {
        Optional<Distributor> byId = distributorRepository.findById(id);
        if (byId.isPresent()) {
            distributorRepository.deleteById(id);
            return new ApiResponse("deleted", true);
        } else {
            return new ApiResponse("not found", false);
        }
    }


}
