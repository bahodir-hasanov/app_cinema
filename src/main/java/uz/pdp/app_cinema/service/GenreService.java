package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.model.Genre;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.GenreRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

// Bahodir Hasanov 3/16/2022 10:06 PM

@Service
@Transactional
public class GenreService {
    @Autowired
    GenreRepository genreRepository;

    public ApiResponse getAllGenres() {
        List<Genre> all = genreRepository.findAll();
        return new ApiResponse("found", true, all);
    }

    public ApiResponse saveGenre(Genre genre) {
        Genre save = genreRepository.save(genre);
        return new ApiResponse("saved", true);
    }

    public ApiResponse getGenreById(UUID id) {
        Optional<Genre> genreById = genreRepository.findById(id);
        if (genreById.isPresent()) {
            return new ApiResponse("found", true, genreById.get());
        }
        return new ApiResponse("not found", false);
    }

    public ApiResponse getDeleteById(UUID id) {
        Optional<Genre> genreById = genreRepository.findById(id);
        if (genreById.isPresent()) {
            genreRepository.deleteById(id);
            return new ApiResponse("deleted", true);
        }
        return new ApiResponse("not found", false);
    }


    public ApiResponse update(UUID id, Genre genre) {
        Optional<Genre> genreById = genreRepository.findById(id);
        if (genreById.isPresent()) {
            Genre genre1 = new Genre();
            genre1.setName(genre.getName());
            return new ApiResponse("update", true);
        }
        return new ApiResponse("not found", false);
    }
}
