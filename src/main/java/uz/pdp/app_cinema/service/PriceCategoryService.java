package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.model.PriceCategory;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.PriceCategoryRepository;

import java.util.Optional;
import java.util.UUID;

// Bahodir Hasanov 3/25/2022 10:12 AM
@Service
@Transactional
public class PriceCategoryService {
    @Autowired
    PriceCategoryRepository priceCategoryRepository;


    public ResponseEntity<?> savePriceCategory(PriceCategory priceCategory) {
        PriceCategory save = priceCategoryRepository.save(priceCategory);
        return ResponseEntity.ok(new ApiResponse("successfully added", true, save));
    }

    public ResponseEntity<?> getAllCategory(int size, int page, boolean sort) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size,
                sort ? Sort.Direction.DESC : Sort.Direction.ASC, "additionalFeeInPercent");
        try {
            Page<PriceCategory> all = priceCategoryRepository.findAll(pageable);
            return ResponseEntity.ok(new ApiResponse("success", true, all));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("problem with data base", false));
        }
    }

    public ResponseEntity<?> getPriceCategoryByID(UUID id) {
        Optional<PriceCategory> optional = priceCategoryRepository.findById(id);
        if (optional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse("success", true, optional.get()));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("not found", false));
    }

    public ResponseEntity<?> deleteById(UUID id) {
        Optional<PriceCategory> optionalById = priceCategoryRepository.findById(id);
        if (optionalById.isPresent()) {
            priceCategoryRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse("successfully delete", true));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("not found", false));
    }

    public ResponseEntity<?> updateById(UUID id, PriceCategory newPriceCategory) {
        Optional<PriceCategory> optionalById = priceCategoryRepository.findById(id);
        if (optionalById.isPresent()) {
            PriceCategory priceCategory = optionalById.get();
            priceCategory.setAdditionalFeeInPercent(newPriceCategory.getAdditionalFeeInPercent());
            priceCategory.setColor(newPriceCategory.getColor());
            priceCategoryRepository.save(priceCategory);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse("updated",true));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("not found", false));
    }
}
