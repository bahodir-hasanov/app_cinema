package uz.pdp.app_cinema.service;
// Bahodir Hasanov 3/24/2022 1:46 PM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.dto.MovieSessionDto;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.projection.MovieSessionProjection;
import uz.pdp.app_cinema.repository.MovieSessionRepository;
import uz.pdp.app_cinema.service.interfaces.MovieSessionService;
@Service
@Transactional
public class MovieSessionServiceImpl implements MovieSessionService {
    @Autowired
    MovieSessionRepository movieSessionRepository;

    @Override
    public ResponseEntity<?> getAllMovieSessions(int page, int size, String search) {
        Pageable pageable  = PageRequest.of(
                page-1,
                size);
        Page<MovieSessionProjection> all = movieSessionRepository.findAllSessionsByPage(
                pageable,
                search);
        return ResponseEntity.ok(new ApiResponse("success", true, all));
    }

    @Override
    public ResponseEntity<?> addMovieSession(MovieSessionDto movieSessionDto) {
        return ResponseEntity.ok("adding movie session id to do!!!");
    }
}
