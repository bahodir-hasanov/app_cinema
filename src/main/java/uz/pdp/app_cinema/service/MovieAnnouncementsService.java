package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.dto.MovieAnnouncementsDto;
import uz.pdp.app_cinema.model.Movie;
import uz.pdp.app_cinema.model.MovieAnnouncement;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.MovieAnnouncementRepository;
import uz.pdp.app_cinema.repository.MovieRepository;

import java.util.Optional;

// Bahodir Hasanov 3/24/2022 11:51 AM
@Service
@Transactional
public class MovieAnnouncementsService {
    @Autowired
    MovieAnnouncementRepository movieAnnouncementRepository;
    @Autowired
    MovieRepository movieRepository;

    public ResponseEntity<?> saveMovieAnnouncement(MovieAnnouncementsDto movieAnnouncementsDto) {
        Optional<Movie> optionalMovie = movieRepository.findById(movieAnnouncementsDto.getMovieId());
        if (optionalMovie.isPresent()) {
            MovieAnnouncement savedMovieAnnouncement = movieAnnouncementRepository.save(new MovieAnnouncement(optionalMovie.get(), movieAnnouncementsDto.isActive()));
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse("successfully saved",true,savedMovieAnnouncement));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("this movie not found", false));
    }
}
