package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.dto.SeatDto;
import uz.pdp.app_cinema.model.PriceCategory;
import uz.pdp.app_cinema.model.Row;
import uz.pdp.app_cinema.model.Seat;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.PriceCategoryRepository;
import uz.pdp.app_cinema.repository.RowRepository;
import uz.pdp.app_cinema.repository.SeatsRepository;

import java.util.Optional;

// Bahodir Hasanov 3/25/2022 6:12 PM
@Service
@Transactional
public class SeatsService {
    @Autowired
    SeatsRepository seatsRepository;
    @Autowired
    PriceCategoryRepository priceCategoryRepository;
    @Autowired
    RowRepository rowRepository;


    public ResponseEntity<?> getAllSeats(int size, int page, boolean direction) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size,
                direction? Sort.Direction.DESC: Sort.Direction.ASC, "priceCategory");
        try {
            Page<Seat> allSeats = seatsRepository.findAll(pageable);
            return ResponseEntity.ok(new ApiResponse("success",true,allSeats));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("success",false));
        }
    }

    public HttpEntity<?> saveSeat(SeatDto seatDto) {
        Optional<PriceCategory> priceCategoryOptional = priceCategoryRepository.findById(seatDto.getPriceCategoryId());
        Optional<Row> rowOptional = rowRepository.findById(seatDto.getRowId());
        if(priceCategoryOptional.isPresent() && rowOptional.isPresent()){
            PriceCategory priceCategory = priceCategoryOptional.get();
            Row row = rowOptional.get();
            Seat save = seatsRepository.save(new Seat(seatDto.getNumber(), row, priceCategory));
            return ResponseEntity.ok(new ApiResponse("successfully added",true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("successfully added",true));
    }
}
