package uz.pdp.app_cinema.service;
// Bahodir Hasanov 3/16/2022 3:50 PM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.app_cinema.dto.MovieDto;
import uz.pdp.app_cinema.dto.MovieFileDto;
import uz.pdp.app_cinema.model.*;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.projection.MovieProjection;
import uz.pdp.app_cinema.repository.*;
import uz.pdp.app_cinema.service.interfaces.MovieService;

import java.io.IOException;
import java.util.*;

@Service
public class MovieServiceImpl implements MovieService {
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    GenreRepository genreRepository;
    @Autowired
    DistributorRepository distributorRepository;
    @Autowired
    CastRepository castRepository;
    @Autowired
    CastRepository getCastRepository;

    @Override
    public ResponseEntity<?> getAllMovies(int page, int size, String search, String sort, boolean direction) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size,
                direction ? Sort.Direction.ASC : Sort.Direction.DESC,
                sort
        );
        try {
            Page<MovieProjection> all = movieRepository.findAllByPage(pageable, search);
            return ResponseEntity.ok(new ApiResponse("success", true, all.getContent()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false, null));
        }
    }

    @Override
    public ResponseEntity<?> getMovieById(UUID id) {
        Optional<Movie> movieOptional = movieRepository.findById(id);
        if (movieOptional.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("found", true, movieOptional.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<?> saveMovie(MovieFileDto file, MovieDto movieDto) throws IOException {
        Attachment save = null;
        List<Attachment> photos = new ArrayList<>();
        Attachment saveCover = null;
        if (!file.equals(null) && !file.getFiles().isEmpty()) {
            List<MultipartFile> files = file.getFiles();
            for (MultipartFile fileI : files) {
                save = attachmentRepository.save(new Attachment(fileI.getContentType(), fileI.getSize(), fileI.getOriginalFilename()));
                attachmentContentRepository.save(new AttachmentContent(save, fileI.getBytes()));
                photos.add(save);
            }
        }
        if (!file.getCoverImage().isEmpty()) {
            MultipartFile coverImage = file.getCoverImage();
            saveCover = attachmentRepository.save(new Attachment(coverImage.getContentType(), coverImage.getSize(), coverImage.getOriginalFilename()));
            attachmentContentRepository.save(new AttachmentContent(saveCover, coverImage.getBytes()));

        }
        Distributor distributor = null;
        Optional<Distributor> byId = distributorRepository.findById(movieDto.getDistributorId());
        if (byId.isPresent()) {
            distributor = byId.get();
        }
        List<Cast> casts = new ArrayList<>();
        for (UUID castId : movieDto.getCasts()) {
            if (castRepository.findById(castId).isPresent()) {
                casts.add(castRepository.findById(castId).get());
            }
        }

        List<Genre> genres = new ArrayList<>();
        for (UUID uuid : movieDto.getGenres()) {
            if (genreRepository.findById(uuid).isPresent()) {
                genres.add(genreRepository.findById(uuid).get());
            }
        }
        try {
            Movie savedMovie = movieRepository.save(new Movie(
                    movieDto.getTitle(),
                    movieDto.getDescription(),
                    movieDto.getDurationInMin(),
                    movieDto.getMinPrice(),
                    saveCover,
                    photos,
                    movieDto.getTrailerVideoUrl(),
                    movieDto.getReleaseDate(),
                    movieDto.getBudget(),
                    distributor, movieDto.getDistributorShareInPercentage(),
                    casts, genres
            ));
            return ResponseEntity.ok(savedMovie);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().build();
        }
    }
}
