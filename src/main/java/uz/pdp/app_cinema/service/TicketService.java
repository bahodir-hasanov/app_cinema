package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.dto.TicketDto;
import uz.pdp.app_cinema.model.*;
import uz.pdp.app_cinema.model.enums.TicketStatus;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.projection.TicketProjection;
import uz.pdp.app_cinema.repository.*;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

// Bahodir Hasanov 3/28/2022 3:10 PM
@Service
@Transactional
public class TicketService {
    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    MovieSessionRepository movieSessionRepository;
    @Autowired
    SeatsRepository seatsRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    NightSessionAddFeeRepository nightSessionAddFeeRepository;
    @Autowired
    AttachmentService attachmentService;
    @Autowired
    TransactionHistoryRepository transactionHistoryRepository;


    public ResponseEntity<?> addTicketToCart(TicketDto ticketDto) {
        try {
            MovieSession movieSession = movieSessionRepository.findById(ticketDto.getMovieSessionId()).orElseThrow(() ->
                    new ResourceNotFoundException("Movie session not found"));
            Seat seat = seatsRepository.findById(ticketDto.getSeatId()).orElseThrow(() ->
                    new ResourceNotFoundException("Seat which is you have selected is not found"));
            Double finalPrice = getTicketFinalPrice(movieSession, seat);
            User user = userRepository.findByUsername("user");
            Attachment attachment = attachmentService.getTicketWithQR(ticketDto.getMovieSessionId());
            Ticket ticket = new Ticket(movieSession, seat, attachment, finalPrice, TicketStatus.NEW, user);
            Ticket savedTicket = ticketRepository.save(ticket);
            schedulesDeleteTicket(savedTicket);
            return ResponseEntity.ok(new ApiResponse("success", true));
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(new ApiResponse("error", false));
        }
    }

    /**
     * this method finds final price of ticket(it depends on vip hall, seat, or movie session time )
     *
     * @param movieSession
     * @param seat
     */

    private Double getTicketFinalPrice(MovieSession movieSession, Seat seat) {
        double minPrice = movieSession.getMovieAnnouncement().getMovie().getMinPrice();
        double seatAdditionalFeeInPercent = seat.getPriceCategory().getAdditionalFeeInPercent();
        double vipAdditionalFeeInPercent = movieSession.getHalls().getVipAdditionalFeeInPercent();
        double nightSessionAdd = 0;
        LocalTime lastTimeChanges = nightSessionAddFeeRepository.getLastTimeChanges();
        LocalTime movieTime = movieSession.getStartTime().getTime();
        if ((movieTime.compareTo(lastTimeChanges) > 0)) {
            nightSessionAdd = nightSessionAddFeeRepository.getLastTimeChangePercentage();
        }
        return minPrice * (1 + (seatAdditionalFeeInPercent + vipAdditionalFeeInPercent + nightSessionAdd) / 100);
    }


    private void schedulesDeleteTicket(Ticket ticket) {
        try {
            //=============== OUR TASK ========================
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    Ticket ticketFromDb = ticketRepository.findById(ticket.getId()).orElseThrow(() ->
                            new ResourceNotFoundException("Ticket not found!!!"));
                    if (ticketFromDb.getStatus().equals(TicketStatus.NEW)) {
                        System.out.println(ticketFromDb.getId() + " Ticket deleted... " + new Date());
                        ticketRepository.delete(ticketFromDb);
                    }
                }
            };
            //=============== GIVEN TIME TO RUN OUR TASK ==================
            Timer timer = new Timer();
            // long delayTime =  THIRTY_MINUTES_IN_MILLISECOND;
            long delayTime = 60000;
            System.out.println("Scheduler started..." + new Date());
            System.out.println("Delay: " + delayTime);
            // GIVEN TASK (timerTask) 15 SECOND (delayTime = 15000)
            timer.schedule(timerTask, delayTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ResponseEntity<?> getTicketsByUserId(UUID userId) {
        List<TicketProjection> ticketsByUserId = ticketRepository.getTicketByUserId(userId);
        return ResponseEntity.ok(new ApiResponse("success", true, ticketsByUserId));
    }

    public boolean changeTicketStatusToPurchase(UUID userId) {
        // TODO: 3/31/2022 get ticket by user id and change their status from new to transaction DONE
        try {
            List<Ticket> tickets = ticketRepository.findTicketsByUserIdAndStatusNew(userId);

            for (Ticket ticket : tickets) {
                ticket.setStatus(TicketStatus.PURCHASED);
                ticketRepository.save(ticket);
            }
//            ticketRepository.saveAll(tickets);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean addToTransactionHistory(UUID userId, String paymentIntent) {

        try {
            List<Ticket> tickets = ticketRepository.findTicketsByUserIdAndStatusNew(userId);

            double totalPrice = tickets.stream().map(Ticket::getPrice).collect(Collectors.toList()).stream().mapToDouble(value -> value).sum();

            TransactionHistory transactionHistory = new TransactionHistory(tickets, null, totalPrice, TicketStatus.PURCHASED, paymentIntent);

            transactionHistoryRepository.save(transactionHistory);

            return true;

        } catch (Exception e) {

            e.printStackTrace();

            return false;
        }

    }
}

