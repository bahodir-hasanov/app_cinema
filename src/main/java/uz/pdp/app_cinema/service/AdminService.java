package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import uz.pdp.app_cinema.repository.TransactionHistoryRepository;

// Bahodir Hasanov 3/30/2022 10:33 AM
@Service
@Transactional
public class AdminService {
    @Autowired
    TransactionHistoryRepository transactionHistoryRepository;

    @GetMapping("/{date}")
    public HttpEntity<?> getContOfAllSoledTicketDaily(@PathVariable String date) {
        Integer countOfAllSoledTicketDaily = transactionHistoryRepository.getCountOfAllSoledTicketDaily(date);
        return ResponseEntity.ok(countOfAllSoledTicketDaily);
    }
}
