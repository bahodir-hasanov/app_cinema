package uz.pdp.app_cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.app_cinema.model.Actor;
import uz.pdp.app_cinema.model.Attachment;
import uz.pdp.app_cinema.model.AttachmentContent;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.ActorRepository;
import uz.pdp.app_cinema.repository.AttachmentContentRepository;
import uz.pdp.app_cinema.repository.AttachmentRepository;

import java.io.IOException;
import java.util.UUID;

// Bahodir Hasanov 3/16/2022 11:34 AM
@Service
@Transactional
public class ActorService {
    @Autowired
    ActorRepository actorRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public ApiResponse getAllActors() {
        return new ApiResponse("found all", true, actorRepository.findAll());
    }

    public ApiResponse saveActor(MultipartFile file, Actor actor) {
        Attachment attachment = new Attachment();
        attachment.setContent_type(file.getContentType());
        attachment.setName(file.getOriginalFilename());
        attachment.setSize(file.getSize());
        AttachmentContent attachmentContent = new AttachmentContent();
        attachmentContent.setAttachment(attachment);
        try {
            attachmentContent.setBytes(file.getBytes());
            attachmentRepository.save(attachment);
            attachmentContentRepository.save(attachmentContent);
            actorRepository.save(new Actor(actor.getFullName(),attachment));
            return new ApiResponse("crated",true);
        } catch (Exception e) {
            return new ApiResponse("sorry",false);
        }
    }

    public ResponseEntity<?> deleteActor(UUID actorId) {
        try {
            actorRepository.deleteById(actorId);
            return ResponseEntity.ok(new ApiResponse ("deleted", true));
        } catch (Exception e){
           return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("actor not found", false));
        }
    }
}
