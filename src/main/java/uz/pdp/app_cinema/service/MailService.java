package uz.pdp.app_cinema.service;

import com.stripe.model.checkout.Session;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import uz.pdp.app_cinema.model.User;
import uz.pdp.app_cinema.repository.UserRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

// Bahodir Hasanov 4/5/2022 9:51 PM
@Service
@RequiredArgsConstructor
public class MailService {
    private final UserRepository userRepository;
    private final TemplateEngine templateEngine;
    private final JavaMailSender javaMailSender;

    public void sendMailMessage(Session session) {
        String userId = session.getClientReferenceId();
        Optional<User> userById = userRepository.findById(UUID.fromString(userId));
        User user = null;
        if (userById.isPresent()) {
            user = userById.get();
        }
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("name", user.getFullName());
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(templateModel);
        String htmlBody = templateEngine.process("ready.html", thymeleafContext);

        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(session.getCustomerDetails().getEmail());
            helper.setSubject("Thank you for your purchase!");
            helper.setText(htmlBody, true);
            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
