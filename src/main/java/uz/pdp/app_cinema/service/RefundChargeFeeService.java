package uz.pdp.app_cinema.service;
// Bahodir Hasanov 4/5/2022 9:37 AM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.app_cinema.dto.RefundFeeDto;
import uz.pdp.app_cinema.model.RefundChargeFee;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.repository.RefundChargeFeeRepository;

@Service
@Transactional
public class RefundChargeFeeService {

    @Autowired
    RefundChargeFeeRepository refundChargeFeeRepository;
    public ResponseEntity<?> saveRefundChargeFee(RefundFeeDto refundFeeDto) {
        RefundChargeFee refundChargeFee = new RefundChargeFee();
        refundChargeFee.setIntervalMinutes(refundFeeDto.getIntervalMinutes());
        refundChargeFee.setPercentage(refundFeeDto.getPercentage());
        RefundChargeFee save = refundChargeFeeRepository.save(refundChargeFee);
        if(save.equals(null)){
            return ResponseEntity.internalServerError().build();
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse("success",true));
    }

    public ResponseEntity<?> getRefundChargeFee(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(
                page-1,
                size
        );
        Page<RefundChargeFee> all = refundChargeFeeRepository.findAll(pageable);
        if (all.getContent().isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponse("nothing yet",false));
        }
        return ResponseEntity.ok(new ApiResponse("success",true,all.getContent()));
    }
}
