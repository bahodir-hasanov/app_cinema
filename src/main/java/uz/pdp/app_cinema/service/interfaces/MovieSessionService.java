package uz.pdp.app_cinema.service.interfaces;
// Bahodir Hasanov 3/16/2022 3:43 PM

import org.springframework.http.ResponseEntity;
import uz.pdp.app_cinema.dto.MovieSessionDto;

public interface MovieSessionService {

    ResponseEntity<?> getAllMovieSessions(
            int page,
            int size,
            String search
    );

    ResponseEntity<?> addMovieSession(MovieSessionDto movieSessionDto);
}
