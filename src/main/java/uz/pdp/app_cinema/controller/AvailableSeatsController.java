package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.app_cinema.projection.AvailableSeatsProjection;
import uz.pdp.app_cinema.repository.SeatsRepository;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/26/2022 10:34

@RestController
@RequestMapping("/api/seat/available-seat")
public class AvailableSeatsController {
    @Autowired
    SeatsRepository seatsRepository;

    @GetMapping("/{movieSessionId}")
    public ResponseEntity<?> getAllAvailableSeatsByMovieSessionId(@PathVariable UUID movieSessionId) {
        List<AvailableSeatsProjection> allAvailableSeatsByMovieSessionId = seatsRepository.findAllAvailableSeatsByMovieSessionId(movieSessionId);
        return ResponseEntity.ok(allAvailableSeatsByMovieSessionId);
    }


}
