package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.app_cinema.model.Cast;
import uz.pdp.app_cinema.payload.ApiResponse;
import uz.pdp.app_cinema.service.CastService;

import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/17/2022 3:29 PM
@RestController
@RequestMapping("/api/cast")
public class CastController {
    @Autowired
    CastService castService;

    @PostMapping
    public HttpEntity<?> saveCast(@RequestPart(name = "file") MultipartFile file, @RequestPart(name = "cast") Cast cast) {
        Cast cast1 = castService.saveCast(file, cast);
        return ResponseEntity.ok(cast1);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCastById(@PathVariable UUID id) {
        Cast castById = castService.getCastById(id);
        if (!castById.equals(null)) {
            ApiResponse apiResponse = new ApiResponse("found", true, castById);
            return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping()
    public HttpEntity<?> getAllCast() {
        List<Cast> casts = castService.getAllCast();
        if (!casts.isEmpty()) {
            ApiResponse apiResponse = new ApiResponse("found", true, casts);
            return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteById(@PathVariable UUID id) {
        boolean b = castService.deleteById(id);
        if (b) {
            return ResponseEntity.status(b ? 200 : 404).build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public HttpEntity<?> update(@RequestPart MultipartFile file, @RequestPart Cast cast){
        try {
            boolean update = castService.update(file, cast);
            return ResponseEntity.status(update?200:409).build();
        } catch (Exception e){

        }
        return HttpEntity.EMPTY;
    }
}
