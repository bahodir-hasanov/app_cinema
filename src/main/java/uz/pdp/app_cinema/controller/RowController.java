package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.app_cinema.model.Row;
import uz.pdp.app_cinema.projection.RowProjection;
import uz.pdp.app_cinema.service.RowService;

import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/15/2022 2:07 PM
@RestController
public class RowController {
    @Autowired
    RowService rowService;

    @GetMapping("api/rows")
    public ResponseEntity<?> getAllRows() {
        List<Row> allRows = rowService.getAllRows();
        return ResponseEntity.ok(allRows);
    }

    @GetMapping("api/rows/{id}")
    public ResponseEntity<?> getAllRowById(@PathVariable UUID id) {
        List<RowProjection> allRows = rowService.getAllRowsById(id);
        return ResponseEntity.ok(allRows);
    }
}
