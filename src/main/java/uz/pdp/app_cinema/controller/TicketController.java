package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.dto.TicketDto;
import uz.pdp.app_cinema.service.TicketService;

import java.util.UUID;

// Bahodir Hasanov 3/28/2022 3:08 PM
@RestController
@RequestMapping("/api/ticket")
public class TicketController {
    @Autowired
    TicketService ticketService;

    @PostMapping()
    public ResponseEntity<?> AddTicketToCart(@RequestBody TicketDto ticketDto) {
        return ticketService.addTicketToCart(ticketDto);
    }

    @GetMapping("/get-my-ticket")
    public ResponseEntity<?> getTicketsByUserId(){
        UUID userId = UUID.fromString("a777f8bf-4ddd-4bfe-97f5-042b2941088b");
        return ticketService.getTicketsByUserId(userId);
    }

}
