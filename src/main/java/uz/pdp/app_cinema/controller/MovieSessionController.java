package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.dto.MovieSessionDto;
import uz.pdp.app_cinema.service.MovieSessionServiceImpl;
import uz.pdp.app_cinema.utils.Constants;

// Bahodir Hasanov 3/24/2022 1:20 PM
@RestController
@RequestMapping("/api/movie-sessions")
public class MovieSessionController {
    @Autowired
    MovieSessionServiceImpl movieSessionService;

    @GetMapping
    public ResponseEntity<?> getAllMovieSessions(
            @RequestParam(name = "size", defaultValue = Constants.DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search
    ) {
        return movieSessionService.getAllMovieSessions(page, size, search);
    }

    @PostMapping
    public ResponseEntity<?> addMovieSession(@RequestBody MovieSessionDto movieSessionDto){
        return movieSessionService.addMovieSession(movieSessionDto);
    }
}
