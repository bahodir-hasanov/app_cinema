package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.dto.HallDto;
import uz.pdp.app_cinema.service.HallService;

// Bahodir Hasanov 3/26/2022 8:39 AM
@RestController
@RequestMapping("/api/halls")
public class HallController {

    @Autowired
    HallService hallService;

    @PostMapping
    public ResponseEntity<?> saveHall(@RequestBody HallDto hallDto){
        return hallService.savaHall(hallDto);
    }

    @GetMapping
    public ResponseEntity<?> getAllHall(){
        return hallService.getAllHall();
    }
}
