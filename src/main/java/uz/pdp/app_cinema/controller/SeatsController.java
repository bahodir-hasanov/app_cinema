package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.dto.SeatDto;
import uz.pdp.app_cinema.model.Seat;
import uz.pdp.app_cinema.service.SeatsService;
import uz.pdp.app_cinema.utils.Constants;

// Bahodir Hasanov 3/25/2022 5:28 PM
@RestController
@RequestMapping("api/seats")
public class SeatsController {

    @Autowired
    SeatsService seatsService;

    @GetMapping
    public ResponseEntity<?> getAllSeats(
            @RequestParam(name = "size", defaultValue = Constants.DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "direction", defaultValue = "true") boolean direction
    ) {
        return seatsService.getAllSeats(size, page, direction);
    }

    @PostMapping
    public HttpEntity<?> savaSeat(@RequestBody SeatDto seatDto) {
        return seatsService.saveSeat(seatDto);
    }


}
