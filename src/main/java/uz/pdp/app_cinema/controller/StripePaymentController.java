package uz.pdp.app_cinema.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Bahodir Hasanov 3/31/2022 8:49 AM

@RestController
@RequiredArgsConstructor
public class StripePaymentController {
    @RequestMapping("/success")
    public ResponseEntity<?> getSuccessMsg(){
        return ResponseEntity.ok("Payment done");
    }
}
