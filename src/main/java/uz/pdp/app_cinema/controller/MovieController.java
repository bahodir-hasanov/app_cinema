package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.dto.MovieDto;
import uz.pdp.app_cinema.dto.MovieFileDto;
import uz.pdp.app_cinema.service.MovieServiceImpl;

import java.io.IOException;
import java.util.UUID;

import static uz.pdp.app_cinema.utils.Constants.DEFAULT_PAGE_SIZE;

// Bahodir Hasanov 3/16/2022 2:41 PM

@RestController
@RequestMapping("/api/movies")
public class MovieController {

    @Autowired
    MovieServiceImpl movieService;

    @GetMapping
    @CrossOrigin
    /**
     * crossOrigin this annotation for connecting with react app
     */
    public HttpEntity<?> getAllMovies(
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search,
            @RequestParam(name = "sort", defaultValue = "title") String sort
    ) {
        return movieService.getAllMovies(page,size,search,sort,true);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<?> saveMovie(MovieFileDto files, @RequestPart("movie") MovieDto movieDto) throws IOException {
           return movieService.saveMovie(files,movieDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMovieById(@PathVariable UUID id){
        return movieService.getMovieById(id);
    }
}
