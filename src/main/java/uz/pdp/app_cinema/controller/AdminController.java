package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.app_cinema.service.AdminService;

// Bahodir Hasanov 3/30/2022 10:28 AM
@RestController
@RequestMapping("api/admin")
public class AdminController {
    @Autowired
    AdminService adminService;

    @GetMapping("/{date}")
    public HttpEntity<?> getCountOfAllSoledTicketDaily(@PathVariable String date) {
        return adminService.getContOfAllSoledTicketDaily(date);
    }


}
