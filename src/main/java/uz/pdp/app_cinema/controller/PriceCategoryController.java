package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.model.PriceCategory;
import uz.pdp.app_cinema.service.PriceCategoryService;
import uz.pdp.app_cinema.utils.Constants;

import java.util.Locale;
import java.util.UUID;

// Bahodir Hasanov 3/25/2022 9:59 AM
@RestController
@RequestMapping("api/price-category")
public class PriceCategoryController {
    @Autowired
    PriceCategoryService priceCategoryService;


    @GetMapping
    public ResponseEntity<?> getAllPriceCategory(
            @RequestParam(name = "size", defaultValue = Constants.DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        return priceCategoryService.getAllCategory(size, page, true);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getPriceCategoryById(@PathVariable(name = "id") UUID id) {
        return priceCategoryService.getPriceCategoryByID(id);
    }


    @PostMapping
    public ResponseEntity<?> savePriceCategory(@RequestBody PriceCategory priceCategory) {
        return priceCategoryService.savePriceCategory(priceCategory);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable(name = "id") UUID id) {
        return priceCategoryService.deleteById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateById(@PathVariable(name = "id") UUID id, @RequestBody() PriceCategory priceCategory) {
        return priceCategoryService.updateById(id, priceCategory);
    }
}
