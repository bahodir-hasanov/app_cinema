package uz.pdp.app_cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.dto.MovieAnnouncementsDto;
import uz.pdp.app_cinema.service.MovieAnnouncementsService;

// Bahodir Hasanov 3/24/2022 11:31 AM
@RestController
@RequestMapping("/api/announcement")
public class MovieAnnouncementController {

@Autowired
MovieAnnouncementsService movieAnnouncementsService;

    @PostMapping
    public ResponseEntity<?> saveMovieAnnouncement(@RequestBody MovieAnnouncementsDto movieAnnouncementsDto){
        return movieAnnouncementsService.saveMovieAnnouncement(movieAnnouncementsDto);
    }
}
