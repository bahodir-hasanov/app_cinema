package uz.pdp.app_cinema.controller;

import com.stripe.exception.SignatureVerificationException;
import com.stripe.model.Event;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_cinema.service.MailService;
import uz.pdp.app_cinema.service.TicketService;

import java.util.UUID;

// Bahodir Hasanov 3/31/2022 9:27 AM
@RestController
@RequestMapping("api/stripe-webhook")
@RequiredArgsConstructor

public class StripeEventController {
    private final TicketService ticketService;
    private final MailService mailService;
    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;
    @Value("${WEBHOOK_SECRET_KEY}")
    String endpointSecret;

    /**
     * stripe listen --forward-to localhost:8080/api/stripe-webhook
     * Dasturda shu yo'lga kelishi uchun ishlatayapmiz stripe qilib beradi mu ishni
     */

    @PostMapping
    public Object handle(@RequestBody String payload, @RequestHeader(name = "Stripe-Signature") String sigHeader) {

        String apikey = stripeApiKey;

        Event event = null;

        try {

            event = Webhook.constructEvent(payload, sigHeader, endpointSecret);

        } catch (SignatureVerificationException e) {

        } catch (Exception e) {

            e.printStackTrace();

        }

        // Handle the checkout.session.completed event
        if ("checkout.session.completed".equals(event.getType())) {
            Session session = (Session) event.getDataObjectDeserializer().getObject().get();
            /**
             *  Fulfill the transaction...
             */
            fulfillOrder(session);
            mailService.sendMailMessage(session);
        }
//        System.out.println("Got payload: " + payload);

        return "";
    }

    public void fulfillOrder(Session session) {
        // TODO: fill me in done
        String currentUserId = session.getClientReferenceId();
        System.out.println("CurrentUser's id: " + currentUserId);
        ticketService.addToTransactionHistory(UUID.fromString(currentUserId), session.getPaymentIntent());
        ticketService.changeTicketStatusToPurchase(UUID.fromString(currentUserId));
    }
}
