package uz.pdp.app_cinema.dto;
// Bahodir Hasanov 3/18/2022 11:48 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieFileDto {
    private MultipartFile coverImage;
    private List<MultipartFile> files;
  //  private String json;

}
