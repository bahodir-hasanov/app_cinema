package uz.pdp.app_cinema.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

// Bahodir Hasanov 3/26/2022 9:02 AM

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieSessionDto {
    private UUID movieAnnouncement;
    private List<ReservedHallDto> reservedHalls;
//    private String endTime; optional
}
