package uz.pdp.app_cinema.dto;
// Bahodir Hasanov 3/26/2022 9:30 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReservedHallDto {
    private UUID hallId;
    private String startDate;
    private String endDate;
    private List<String> startTimeList;
}
