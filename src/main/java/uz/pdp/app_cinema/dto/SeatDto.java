package uz.pdp.app_cinema.dto;
// Bahodir Hasanov 3/25/2022 11:40 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SeatDto {
    private Integer number;
    private UUID rowId;
    private UUID priceCategoryId;
}
