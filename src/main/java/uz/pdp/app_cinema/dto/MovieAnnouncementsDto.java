package uz.pdp.app_cinema.dto;
// Bahodir Hasanov 3/24/2022 11:25 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;
@AllArgsConstructor
@Data
@NoArgsConstructor
public class MovieAnnouncementsDto {
    private UUID movieId;
    private boolean isActive;
}
