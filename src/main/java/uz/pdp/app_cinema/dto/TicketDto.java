package uz.pdp.app_cinema.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

// Bahodir Hasanov 3/28/2022 3:13 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TicketDto {
    private UUID ticketId;
    private UUID movieSessionId;
    private UUID seatId;
}
