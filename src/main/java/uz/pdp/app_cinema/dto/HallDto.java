package uz.pdp.app_cinema.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Bahodir Hasanov 3/26/2022 8:45 AM
@AllArgsConstructor
@NoArgsConstructor
@Data
public class HallDto {
    private String name;
    private double vipAdditionalFeeInPercent;
}
